var katescript = {
	"author": "Adam Hlavatovic <adam.hlavatovic@protonmail.ch>",
	"revision": 1,
	"kate-version": "5.0",
	"functions": ["add_line", "add_line_before", "foo"],
	"actions": [
		{
			"function": "add_line",
			"name": "Add Line",
			"category": "Editing",
			"shortcut": "Ctrl+Return",
			"interactive": "false"
		},
		{
			"function": "add_line_before",
			"name": "Add Line Before",
			"category": "Editing",
			"shortcut": "Ctrl+Shift+Return",
			"interactive": "false"
		},
		{
			"function":"foo",
			"name":"foo",
			"category":"Editing",
			"interctive":"false"
		}
	]
};

require("range.js");

function add_line()
{
	document.editBegin();
	var cur = view.cursorPosition();
	var first_char_pos = document.firstColumn(cur.line);
	var ident = document.text(cur.line, 0, cur.line, first_char_pos);
	document.insertLine(cur.line+1, ident);
	view.setCursorPosition(cur.line+1, first_char_pos)
	document.editEnd();
}

function add_line_before()
{
	document.editBegin();
	var cur = view.cursorPosition();
	var first_char_pos = document.firstColumn(cur.line);
	var ident = document.text(cur.line, 0, cur.line, first_char_pos);
	document.insertLine(cur.line, ident);
	view.setCursorPosition(cur.line, first_char_pos)
	document.editEnd();
}

function foo()
{
	// playground for testing ...
}
